/**
 * gcal.js
 * https://bitbucket.org/pgrippi/gcal.js
 *
 * Copyright 2012 Peter Grippi
 * Released under the MIT license
 * https://bitbucket.org/pgrippi/gcal.js/raw/master/LICENSE
 */
;(function() {
  "use strict";

  //
  // Constants
  //

  /**
   * @type {Function}
   * @const
   */
  var slice = Array.prototype.slice;

  /**
   * @type {Function}
   * @const
   */
  var nativeTrim = String.prototype.trim;

  /**
   * @type {Object}
   * @const
   */
  var COUNTDOWN_DEFAULTS = {
    'format'       : 'text',
    'separator'    : ', ',
    'showZeroDays' : false
  };

  //
  // gcal
  //

  /**
   * Creates a Google Calendar with the given feed URL
   *
   * @param {string} feedUrl the URL to the public feed
   * @return {GoogleCalendar} a GoogleCalendar instance
   */
  function gcal(feedUrl) {
    return new GoogleCalendar(feedUrl);
  }

  //
  // GoogleCalendar Class
  //

  /**
   * Creates a Google Calendar with the given feed URL
   *
   * @constructor
   * @param {string} feedUrl the URL to the public feed
   */
  function GoogleCalendar(feedUrl) {
    /**
     * @type {string}
     * @private
     */
    this.feedUrl = feedUrl.replace(/\/public\/basic$/i, '/public/full').replace(/^http:/i, 'https:');
  }

  /**
   * Creates a countdown to the next event in the Google calendar
   *
   * @param {Element} element the HTML DOM element
   * @param {Object=} options the options for the countdown
   * @return {GoogleCalendar} this Google Calendar
   */
  GoogleCalendar.prototype.countdown = function(element, options) {
    // normalize the options
    options = defaults(options || {}, COUNTDOWN_DEFAULTS);
    options['format'] = options['format'].toLowerCase();

    // clear out any garbage inside the element
    element = clearNode(element);

    // add some CSS
    addClass(element, 'gcaljs gcaljs-loading gcaljs-' + options['format']);

    // set the loading message
    element.appendChild(text('Loading Calendar Event List...'));

    // get all events starting today and going forward
    var today = new Date(), self = this;
    today.setHours(0, 0, 0, 0);

    this.getEvents(
      today,
      /**
       * @param {Array.<CalendarEntry>} events
       */
      function(events) {
        var targetEvent,
            now = new Date(),
            times, startTime, endTime;

        // remove the class we added
        removeClass(element, 'gcaljs-loading');

        // find the first non-all day event
        each(events,
          /**
           * @param {CalendarEntry} calendarEvent
           * @return {boolean|undefined}
           */
          function(calendarEvent) {
            if (calendarEvent.isAllDay()) {
              // skip all day events
              return;
            }

            startTime = calendarEvent.getStartTime();
            endTime = calendarEvent.getEndTime();
            if ((now > startTime && now < endTime) || now < startTime) {
              targetEvent = calendarEvent;
              return false;
            }
          });

        // were we able to find any events?
        if (targetEvent === undefined) {
          element.firstChild.textContent = 'No upcoming events.';
          addClass(element, 'gcaljs-noevents');
          return;
        }

        var title = targetEvent.getTitle();

        // set up the HTML if the format selected is HTML
        var title_span, verb_span,
            days_span, days_num_span, days_noun_span,
            hours_span, hours_num_span, hours_noun_span,
            minutes_span, minutes_num_span, minutes_noun_span,
            seconds_span, seconds_num_span, seconds_noun_span;
        if (options['format'] === 'html') {
          element = clearNode(element);

          // title
          title_span = createElement('span', { 'className' : 'gcaljs-event-title' });
          title_span.appendChild(text(title));

          // starts or ends
          verb_span = createElement('span', { 'className' : 'gcal-js-event-status' }, [text('')]);

          // days
          days_span = createElement('span', { 'className' : 'gcaljs-event-days' }, [
            (days_num_span = createElement('span', { 'className': 'gcal-js-num' }, [text('')])),
            text(' '),
            (days_noun_span = createElement('span', { 'className' : 'gcal-js-noun' }, [text('')]))
          ]);

          // hours
          hours_span = createElement('span', { 'className' : 'gcaljs-event-hours' }, [
            (hours_num_span = createElement('span', { 'className': 'gcal-js-num' }, [text('')])),
            text(' '),
            (hours_noun_span = createElement('span', { 'className' : 'gcal-js-noun' }, [text('')]))
          ]);

          // minutes
          minutes_span = createElement('span', { 'className' : 'gcaljs-event-minutes' }, [
            (minutes_num_span = createElement('span', { 'className': 'gcal-js-num' }, [text('')])),
            text(' '),
            (minutes_noun_span = createElement('span', { 'className' : 'gcal-js-noun' }, [text('')]))
          ]);

          // seconds
          seconds_span = createElement('span', { 'className' : 'gcaljs-event-seconds' }, [
            (seconds_num_span = createElement('span', { 'className': 'gcal-js-num' }, [text('')])),
            text(' '),
            (seconds_noun_span = createElement('span', { 'className' : 'gcal-js-noun' }, [text('')]))
          ]);

          each(
            [
              title_span,
              text(' '),
              verb_span,
              text(' '),
              days_span,
              createElement('span', { 'className': 'gcal-js-separator' }, [text(options['separator'])]),
              hours_span,
              createElement('span', { 'className': 'gcal-js-separator' }, [text(options['separator'])]),
              minutes_span,
              createElement('span', { 'className': 'gcal-js-separator' }, [text(options['separator'])]),
              seconds_span
            ],
            /**
             * @param {Element} node
             */
            function(node) {
              element.appendChild(node);
            }
          );
        }

        var countdownInterval, countdownFn = function() {
          var timeDiff = diff(startTime);
          var inProgress = false;

          // check if we are passed the start date
          if (timeDiff.completed) {
            timeDiff = diff(endTime);
            inProgress = true;
          }

          // check if we are passed the end date
          if (timeDiff.completed) {
            window.clearInterval(countdownInterval);
            self.countdown(element, options);
            return;
          }

          var inProgressVerb = (inProgress ? 'ends' : 'starts') + ' in';

          if (timeDiff.days === 0) {
            addClass(element, 'gcaljs-zero-days');
          } else {
            removeClass(element, 'gcaljs-zero-days');
          }

          if (options['format'] === 'html') {
            if (inProgress) {
              addClass(verb_span, 'gcaljs-event-inprogress');
              removeClass(verb_span, 'gcaljs-event-future');
            } else {
              removeClass(verb_span, 'gcaljs-event-inprogress');
              addClass(verb_span, 'gcaljs-event-future');
            }

            verb_span.firstChild.textContent = inProgressVerb;
            days_num_span.firstChild.textContent = timeDiff.days;
            days_noun_span.firstChild.textContent = 'day' + (timeDiff.days === 1 ? '' : 's');
            hours_num_span.firstChild.textContent = timeDiff.hours;
            hours_noun_span.firstChild.textContent = 'hour' + (timeDiff.hours === 1 ? '' : 's');
            minutes_num_span.firstChild.textContent = timeDiff.minutes;
            minutes_noun_span.firstChild.textContent = 'minute' + (timeDiff.minutes === 1 ? '' : 's');
            seconds_num_span.firstChild.textContent = timeDiff.seconds;
            seconds_noun_span.firstChild.textContent = 'second' + (timeDiff.seconds === 1 ? '' : 's');
          } else {
            element.firstChild.textContent = sprintf(
              '{0} {1} {2}{3} hour{4}{5}{6} minute{7}{8}{9} second{10}',
              title,
              inProgressVerb,
              (timeDiff.days > 0 || options['showZeroDays']) ? sprintf('{0} day{1}{2}', timeDiff.days, timeDiff.days === 1 ? '' : 's', options['separator']) : '',
              timeDiff.hours,
              timeDiff.hours === 1 ? '' : 's',
              options['separator'],
              timeDiff.minutes,
              timeDiff.minutes === 1 ? '' : 's',
              options['separator'],
              timeDiff.seconds,
              timeDiff.seconds === 1 ? '' : 's'
            );
          }
        };
        countdownInterval = window.setInterval(countdownFn, 1000);
        countdownFn();
      }
    );

    return this;
  };

  /**
   * Returns the events for the calendar, executing the callback function
   * and passing the events as the first parameter
   *
   * @param {(Function|Date)} minStartTime the minimum start time for the events returned
   * @param {(Function|Date)=} maxStartTime the maximum start time for the events returned
   * @param {Function=} callback the callback function to execute when the events are loaded
   * @suppress {checkTypes}
   */
  GoogleCalendar.prototype.getEvents = function(minStartTime, maxStartTime, callback) {
    var self = this;
    var globalCallbackName = 'gcaljs_' + uuid();

    if (isType(minStartTime, 'Function')) {
      // getEvents(callback)

      callback = minStartTime;
      minStartTime = null;
      maxStartTime = null;
    } else if (isType(maxStartTime, 'Function')) {
      // getEvents(minStartTime, callback)

      callback = maxStartTime;
      maxStartTime = null;
    } else {
      // getEvents(minStartTime, maxStartTime, callback)

      if (!isType(minStartTime, 'Date')) {
        throw "invalid minStartTime";
      } else if (!isType(maxStartTime, 'Date')) {
        throw "invalid maxStartTime";
      } else if (!isType(callback, 'Function')) {
        throw "invalid callback";
      }
    }

    var s, scriptTag, params = {
      'sortorder'  : 'ascending',
      'alt'        : 'json-in-script',
      'user-agent' : 'gcal.js',
      'callback'   : globalCallbackName
    };
    if (minStartTime != null) {
      params['start-min'] = dateToISO8601(minStartTime);
    }
    if (maxStartTime != null) {
      params['start-max'] = dateToISO8601(maxStartTime);
    }
    
    window[globalCallbackName] = function(data) {
      window[globalCallbackName] = undefined;
      removeChild(scriptTag);
      callback.call(window, parseGoogleFeeds(data));
    };

    scriptTag = document.createElement('script');
    scriptTag['type'] = "text/javascript";
    scriptTag['src'] = this.feedUrl + '?' + makeParams(params);

    s = document.getElementsByTagName("script")[0];
    s.parentNode.insertBefore(scriptTag, s);
  }

  //
  // DateDiff Class
  //

  /**
   * Difference in time
   *
   * @constructor
   * @param {number} days the number of days
   * @param {number} hours the number of hours
   * @param {number} minutes the number of minutes
   * @param {number} seconds the number of seconds
   */
  function DateDiff(days, hours, minutes, seconds) {
    this.days = days;
    this.hours = hours;
    this.minutes = minutes;
    this.seconds = seconds;
    this.completed = days === 0 && hours === 0 && minutes === 0 && seconds === 0;
  }

  /**
   * Returns a DateDiff that represents a finished countdown
   *
   * @return {DateDiff} finished DateDiff
   */
  DateDiff.done = function() {
    return new DateDiff(0, 0, 0, 0);
  }

  //
  // CalendarEntry Class
  //

  /**
   * Creates a new Calendar Entry object
   *
   * @constructor
   * @param {Object.<string, string>} gCalEntry the raw data from the Google API
   */
  function CalendarEntry(gCalEntry) {
    var when = gCalEntry['gd$when'] || [];

    this.allDay = false;
    if (when.length > 0) {
      this.eventStartTime = ISO8601ToDate(when[0]['startTime']);
      this.eventEndTime = ISO8601ToDate(when[0]['endTime']);
      this.allDay = /^\d{4}-\d{2}-\d{2}$/.test(when[0]['startTime']);
    } else {
      this.eventStartTime = null;
      this.eventEndTime = null;
    }
    this.eventTitle = gCalEntry['title']['$t'];
  }

  /**
   * Returns the start time of the event
   *
   * @return {Date} the event start time
   */
  CalendarEntry.prototype.getStartTime = function() {
    return this.eventStartTime;
  }

  /**
   * Returns the end time of the event
   *
   * @return {Date} the event end time
   */
  CalendarEntry.prototype.getEndTime = function() {
    return this.eventEndTime;
  }

  /**
   * Returns the title of the event
   *
   * @return {string} the event title
   */
  CalendarEntry.prototype.getTitle = function() {
    return this.eventTitle || '(No title)';
  }

  /**
   * Returns whether or not the event is an all day event
   *
   * @return {boolean} true if the event is all day, false otherwise
   */
  CalendarEntry.prototype.isAllDay = function() {
    return this.allDay;
  }

  //
  // DOM Operations
  //

  /**
   * Clears the contents of a DOM element. This is done by doing a shallow copy
   * (a copy that clones only the node passed in), and then replacing the existing node
   * with the new clone.
   *
   * @param {Element} elem the HTML DOM element to clear the contents of
   * @return {Element} the new HTML DOM element
   */
  function clearNode(elem) {
    var copy = elem.cloneNode(false);
    elem.parentNode.insertBefore(copy, elem);
    removeChild(elem);

    return copy;
  }

  /**
   * Removes an element from the DOM tree
   *
   * @param {Element} elem the HTML DOM element to remove
   */
  function removeChild(elem) {
    elem.parentNode.removeChild(elem);
  }

  /**
   * Creates a new text node with the given text
   *
   * @param {*} txt the text for the node
   * @return {Node} the text node
   */
  function text(txt) {
    return document.createTextNode('' + txt);
  }

  //
  // Utilities
  //

  /**
   * Proxys a method to the given context
   *
   * @param {Function} fn
   * @param {Object} context
   * @return {Function}
   */
  function proxy(fn, context) {
    var args = slice.call(arguments, 2);

    return function() {
      return fn.apply(context, args.concat(slice.call(arguments)));
    };
  }

  /**
   * Iterates over the object passed in, calling the iterator method
   * and passing in the value, key, and object in that order.
   *
   * @param {Array|Object} obj the object to iterate over
   * @param {Function} iterator the function to call
   * @param {Object=} context the context to execute the iterator method in
   */
  function each(obj, iterator, context) {
    if (obj == null) {
      return;
    }

    if (obj.length === +obj.length) {
      for (var i = 0, l = obj.length; i < l; ++i) {
        if (iterator.call(context, obj[i], i, obj) === false) {
          return;
        }
      }
    } else {
      for (var key in obj) {
        if (obj.hasOwnProperty(key)) {
          if (iterator.call(context, obj[key], key, obj) === false) {
            return;
          }
        }
      }
    }
  }

  /**
   * Fills in a given object with default properties.
   *
   * @param {Object} obj the object to work on
   * @param {...Object} var_args the defaults
   * @return {Object} the object
   */
  function defaults(obj, var_args) {
    each(slice.call(arguments, 1), function(source) {
      for (var prop in source) {
        if (source.hasOwnProperty(prop) && obj[prop] == null) {
          obj[prop] = source[prop];
        }
      }
    });
    return obj;
  }

  /**
   * Formats a string with the given values.
   *
   * This method takes two or more arguments. The first argument is the format string,
   * which should contain values such as: "{0}", "{1}", ...
   * These values get replaced with the arguments following the format string.
   *
   * @param {...string} var_args the format string, followed by the replacements
   * @return {string} the formatted string
   */
  function sprintf(var_args) {
    var args = slice.call(arguments),
        str = args[0],
        len, i;

    for (i = 1, len = args.length; i < len; ++i) {
      str = str.replace(RegExp('\\{' + (i - 1) + '\\}', 'g'), args[i]);
    }

    return str;
  }

  /**
   * Returns a DateDiff object with the difference between now and the given time
   *
   * @param {Date} targetDate the target date
   * @return {DateDiff} the DateDiff object representing the difference between now and the given time
   */
  function diff(targetDate) {
    var diff = (targetDate - (new Date().getTime())) / 1000, seconds, minutes, hours, days;

    if (diff <= 0) {
      return DateDiff.done();
    }

    days = Math.floor(diff / (60 * 60 * 24)) % 100000;
    hours = Math.floor(diff / (60 * 60)) % 24;
    minutes = Math.floor(diff / 60) % 60;
    seconds = Math.floor(diff % 60);

    return new DateDiff(days, hours, minutes, seconds);
  }

  /**
   * Checks for if the object passed in is an instance of the type passed in
   *
   * @param {?} object the object to check
   * @param {string} type the type of object to check for
   * @return {boolean} whether or not the object is the type specified
   */
  function isType(object, type) {
    return object != null && Object.prototype.toString.call(object) === '[object ' + type + ']';
  }

  /**
   * Left-pads the passed in string with a zero if it is less than 10
   *
   * @param {number} n the number to pad
   * @return {string} the padded string
   */
  function zeroPad(n) {
    return (n < 10 ? '0' : '') + n;
  }

  /**
   * Returns an ISO 8601 date string from a JavaScript date object
   * Adapted from http://delete.me.uk/2005/03/iso8601.html, released under the AFL
   *
   * @param {Date} date the date to format
   * @return {string} the ISO 8601 date
   */
  function dateToISO8601(date) {
    return [
      date.getUTCFullYear(),
      '-',
      zeroPad(date.getUTCMonth() + 1),
      '-',
      zeroPad(date.getUTCDate()),
      'T',
      zeroPad(date.getUTCHours()),
      ':',
      zeroPad(date.getUTCMinutes()),
      ':',
      zeroPad(date.getUTCSeconds()),
      'Z'
    ].join('');
  }

  /**
   * Returns a Date object from the given ISO 8601 date string
   * Adapted from http://delete.me.uk/2005/03/iso8601.html, released under the AFL
   *
   * @param {string} string the ISO 8601 date
   * @return {Date} the date object
   */
  function ISO8601ToDate(string) {
    var d = string.match(/([0-9]{4})(-([0-9]{2})(-([0-9]{2})(T([0-9]{2}):([0-9]{2})(:([0-9]{2})(\.([0-9]+))?)?(Z|(([-+])([0-9]{2}):([0-9]{2})))?)?)?)?/);

    var offset = 0, time;
    var date = new Date(int10(d[1]), 0, 1);

    // month
    if (d[3]) {
      date.setMonth(int10(d[3]) - 1);
    }

    // day of month
    if (d[5]) {
      date.setDate(int10(d[5]));
    }

    // hours
    if (d[7]) {
      date.setHours(int10(d[7]));
    }

    // minutes
    if (d[8]) {
      date.setMinutes(int10(d[8]));
    }

    // seconds
    if (d[10]) {
      date.setSeconds(int10(d[10]));
    }

    // milliseconds
    if (d[12]) {
      date.setMilliseconds(parseFloat("0." + d[12]) * 1000);
    }

    // timezone offset
    if (d[14]) {
      offset = (int10(d[16]) * 60) + int10(d[17]);
      offset *= ((d[15] === '-') ? 1 : -1);
    }

    offset -= date.getTimezoneOffset();
    time = (date.getTime() + (offset * 60 * 1000));

    date.setTime(time);

    return date;
  }

  /**
   * Returns a random and unique identifier
   *
   * @return {string} unique identifier
   */
  function uuid() {
    var parts = [];
    for (var i = 0; i < 8; ++i) {
      parts.push(Math.floor(Math.random() * 65536).toString(16));
    }
    return parts.join('');
  }

  /**
   * Builds a string of query string parameters out of a javascript hash object
   *
   * @param {Object.<string, string>} params the params hash
   * @return {string} the query string generated from the params hash
   */
  function makeParams(params) {
    var keyValPairs = [];
    each(params, function(param, key) {
      keyValPairs.push(encodeURIComponent(key) + "=" + encodeURIComponent(param));
    });

    return keyValPairs.join("&").replace(/%20/g, "+");
  }

  /**
   * Creates CalendarEntry objects out of the raw Google Calendar feed
   *
   * @param {Object.<string, string>} root the JSON object returned by the Google API
   * @return {Array.<CalendarEntry>} the calendar entries in the Google Calendar
   */
  function parseGoogleFeeds(root) {
    var entries = [];

    each(root['feed']['entry'] || [], function(entry) {
      entries.push(new CalendarEntry(entry));
    });

    return entries;
  }

  /**
   * Converts a value into an integer (base 10)
   * Wrapper around parseInt(x, 10)
   *
   * @param {*} x the value to convert into an integer
   * @return {number} the integer parsed from x, or NaN
   */
  function int10(x) {
    return parseInt(x, 10);
  }

  /**
   * Adds class(es) to the given element
   *
   * @param {Element} elem the element to add the class to
   * @param {string} klass the class(es) to add (space separated)
   */
  function addClass(elem, klass) {
    var classNames = (klass || "").split(/\s+/), setClass;
    if (elem && elem.nodeType === 1) {
      if (!elem.className && classNames.length === 1) {
        elem.className = klass;
      } else {
        setClass = " " + elem.className + " ";
        each(classNames, function(cn) {
          if (setClass.indexOf(" " + cn + " ") < 0) {
            setClass += cn + " ";
          }
        });
        elem.className = trim(setClass);
      }
    }
  }

  /**
   * Removes class(es) from the given element
   *
   * @param {Element} elem the element to remove the class from
   * @param {string} klass the class(es) to remove (space separated)
   */
  function removeClass(elem, klass) {
    var classNames = (klass || "").split(/\s+/), className;
    if (elem && elem.nodeType === 1 && elem.className) {
      className = (" " + elem.className + " ").replace(/[\t\r\n]/g, " ");
      each(classNames, function(cn) {
        while (className.indexOf(" " + cn + " ") >= 0) {
          className = className.replace(" " + cn + " " , " ");
        }
      });
      elem.className = klass ? trim(className) : "";
    }
  }

  /**
   * Creates a DOM element with the given tags and attributes
   *
   * @param {string} tag the tag name of the HTML DOM element to create
   * @param {Object=} attributes the attributes to assign to the DOM element (undefined for none)
   * @param {Array.<Element>=} children an array of children for the DOM element (undefined for none)
   * @return {Element} the created HTML DOM element
   */
  function createElement(tag, attributes, children) {
    var html_tag = document.createElement(tag);
    each(attributes || {}, function(value, key) {
      if (key === 'className') {
        html_tag.className = value;
      } else {
        html_tag.setAttribute(key, value);
      }
    });
    each(children || [], function(child) {
      html_tag.appendChild(child);
    });
    return html_tag;
  }

  var trim = nativeTrim && !nativeTrim.call("\uFEFF\xA0") ?
    /**
     * @param {string} text
     * @return {string}
     */
    function(text) {
      return text == null ? "" : nativeTrim.call(text);
    } :
    /**
     * @param {string} text
     * @return {string}
     */
    function(text) {
      return text == null ? "" : ("" + text).replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, "");
    };

  //
  // Exports
  //

  // globals
  window['gcal'] = gcal;

  // instance methods (GoogleCalendar)
  GoogleCalendar.prototype['getEvents'] = GoogleCalendar.prototype.getEvents;
  GoogleCalendar.prototype['countdown'] = GoogleCalendar.prototype.countdown;

  // instance methods (CalendarEntry)
  CalendarEntry.prototype['getStartTime'] = CalendarEntry.prototype.getStartTime;
  CalendarEntry.prototype['getEndTime'] = CalendarEntry.prototype.getEndTime;
  CalendarEntry.prototype['getTitle'] = CalendarEntry.prototype.getTitle;
  CalendarEntry.prototype['isAllDay'] = CalendarEntry.prototype.isAllDay;
}());