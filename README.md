gcal.js
=======

gcal.js provides a simple read-only interface for public-facing Google Calendars.

## Usage

Download the [minified library](https://bitbucket.org/pgrippi/gcal.js/raw/master/gcal.min.js) and include it in your html.

    :::html
    <script src="gcal.min.js"></script>

### gcal()

To start, use `gcal()` to create an instance of the Google Calendar. You will first need to get the ID of the Google Calendar, which can be found by clicking on "Calendar settings" next to the calendar you wish to use. The Calendar ID is then shown beside "Calendar Address".

    :::javascript
    var my_calendar = gcal('https://www.google.com/calendar/feeds/<ID>/public/full');

The object returned from `gcal()` has two methods, `getEvents()` and `countdown()`.

### getEvents()

The method `getEvents()` takes one, two, or three parameters:

    :::javascript
    getEvents(minStartTime, maxStartTime, callback)
    getEvents(minStartTime, callback)
    getEvents(callback)


### `minStartTime`

A `Date` object which will be used to specify the minimum start time for the returned events. Any events on the calendar that start before this value will be ignored.

### `maxStartTime`

A `Date` object which will be used to specify the maximum start time of the returned events. Any events on the calendar that start after this value will be ignored.

### `callback`

A `function` that will be called when the calendar events are loaded. Due to the way this library loads the events, a blocking call is not possible. The function should accept one argument, which will be an array containing all of the events, in ascending order of start times.

All elements in the array will be instances of `CalendarEntry`. See below for details on this class.

### Example usage

    :::javascript
    var my_calendar = gcal('https://www.google.com/calendar/feeds/<ID>/public/full');
    
    my_calendar.getEvents(new Date(2012, 10, 1), function(events) {
      alert('There are ' + events.length + ' events on my calendar that started on or after Nov 1, 2012');
      if (events.length > 0) {
        alert('The first event has a title of ' + events[0].getTitle());
      }
    });

### countdown()

The method `countdown()` takes one or two parameters:

    :::javascript
    countdown(element)
    countdown(element, options)

### `element`

The container to place the countdown. This parameter must be an HTML DOM element (i.e., the return value of `document.getElementById`).

**WARNING:** The container will be cleared of all content upon initialization.

### `options`

An `Object` who overrides any of the default options, which are:

    :::javascript
    {
      format       : 'text',
      separator    : ', ',
      showZeroDays : false
    }

`format`

The format to use for the countdown. Possible options are `html` and `text`.

If `html` is used, the following HTML will be inserted into the element:

    :::html
    <span class="gcaljs-event-title">TITLE</span>
    <span class="gcal-js-event-status">starts in</span>
    <span class="gcaljs-event-days">
      <span class="gcal-js-num">1</span>
      <span class="gcal-js-noun">day</span>
    </span>
    <span class="gcal-js-separator">, </span>
    <span class="gcaljs-event-hours">
      <span class="gcal-js-num">1</span>
      <span class="gcal-js-noun">hour</span>
    </span>
    <span class="gcal-js-separator">, </span>
    <span class="gcaljs-event-minutes">
      <span class="gcal-js-num">1</span>
      <span class="gcal-js-noun">minute</span>
    </span>
    <span class="gcal-js-separator">, </span>
    <span class="gcaljs-event-seconds">
      <span class="gcal-js-num">1</span>
      <span class="gcal-js-noun">second</span>
    </span>

`separator`

The spacer inbetween the days, hours, minutes, and seconds.

`showZeroDays` (format "text" only)

If set to `true` the days remaining will always show. The default behavior is to hide the days remaining if the days remaining is 0.

## The CalendarEntry class

The CalendarEntry class has the following methods defined on it:

### `getStartTime()`

returns a `Date` object that is the start time for the event.

### `getEndTime()`

returns a `Date` object that is the time which the event is due to finish.

### `getTitle()`

returns the title of the event

### `isAllDay()`

returns `true` if the event is an all-day event, or `false` if it is not

## License

gcal.js is is licensed under the terms of the MIT License, see the included LICENSE file.